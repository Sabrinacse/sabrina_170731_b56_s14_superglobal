<?php
session_start();
if(isset($_POST['firstName'] )) {

    $_SESSION['firstName'] = $_POST['firstName'];
    $_SESSION['lastName'] = $_POST['lastName'];
    $_SESSION['ID'] = $_POST['ID'];
    $_SESSION['bloodGroup'] = $_POST['bloodGroup'];
}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        td{
            text-align: center;
        }
    </style>
</head>
<body>
<table border="1px" align="center">

    <tr> <td colspan="4">Person Information</tr>

    <tr>
        <th> First Name </th>
        <th> Last Name </th>
        <th> ID </th>
        <th> Blood Group </th>
    </tr>

    <tr>
        <td> <?php echo $_SESSION['firstName']?> </td>
        <td><?php echo $_SESSION['lastName']?> </td>
        <td> <?php echo $_SESSION['ID']?></td>
        <td> <?php echo $_SESSION['bloodGroup']?> </td>
    </tr>


</table>
</body>
</html>

